/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompany.boutique;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author fliorentino
 */
public class Personne {
    public int id ;
    public String nom ;
    public String prenom;
    public LocalDate dateNaissance ;
    
    public Personne(){
        
    }
    public Personne(int id,String nom, String prenom, LocalDate dateNaissance){
        this.id = id;
        this.nom =nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }
    public Personne(String nom, String prenom, LocalDate dateNaissance){
        this.nom =nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    
    
    public int getAge(){
        Date date = new Date(); 
        int age ;
        age = (date.getYear()- dateNaissance.getYear() );
        return age ;
    }
    public int getAge(LocalDate ref){
        int age ;
        age = (ref.getYear()- dateNaissance.getYear() );
        return age;
    }
    
    @Override
    public String toString(){
       return "Personne : { id= "+ id + "Nom="+ nom + "Prénom="+ prenom +"Date de naissance="+ dateNaissance;
    }
    
    @Override
    public int hashCode(){
        int hash = 7 ;
         hash = 75*hash + Objects.hashCode(this.id) ;
        return hash ;
    }
    @Override
    public boolean equals(Object obj){
        if(obj == this)
            return true;
        if(obj == null)
            return false;
        if(this.getClass()!=obj.getClass())
            return false;
        Personne other = new Personne();
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
       return true;
    }
    
    public static void main(String[] args){
        
    }
};
