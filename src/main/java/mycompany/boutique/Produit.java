/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompany.boutique;

import java.time.LocalDate;

/**
 *
 * @author fliorentino
 */
public class Produit

{
    private int id;
    private int id_produitAchete;
    private double prixUnitaire;
    private String libelle;
    private LocalDate datePeremption;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_produitAchete() {
        return id_produitAchete;
    }

    public void setId_produitAchete(int id_produitAchete) {
        this.id_produitAchete = id_produitAchete;
    }

    public double getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public LocalDate getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }

    public boolean estPerime(){
        return true;
    }

    public boolean estPerime(LocalDate ref){
        return true;
    }
            
}
