/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompany.boutique;

import java.time.LocalDate;

/**
 *
 * @author fliorentino
 */
public class Client extends Personne{
    private String carteVisa ;
    
    public Client(){
        
    }
    public Client( String carteVisa, int id, String nom, String prenom, LocalDate dateNaissance){
        super(id,nom,prenom,dateNaissance);
        this.carteVisa = carteVisa;
    }

    public String getCarteVisa() {
        return carteVisa;
    }

    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }   
}