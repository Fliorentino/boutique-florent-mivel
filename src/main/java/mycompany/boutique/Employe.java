/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompany.boutique;

import java.time.LocalDate;

/**
 *
 * @author fliorentino
 */
public class Employe extends Personne {
    private String cnss ;
    
    public Employe(String cnss){
        this.cnss = cnss;
    }
    public Employe(String nom, String prenom, LocalDate dateNaissance, String cnss){
        super(nom,prenom,dateNaissance);
        this.cnss = cnss;
    }

    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }
}
