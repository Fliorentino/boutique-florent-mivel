/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompany.boutique;

import java.time.LocalDate;

/**
 *
 * @author fliorentino
 */
public class Achat {
    private int id ;
    private double remise = 0 ;
    private LocalDate dateAchat;
    
    public Achat(){
        
    }
    public Achat(int id, double remise, LocalDate dateAchat){
        this.id = id;
        this.remise = remise ;
        this.dateAchat = dateAchat ;
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }
    
    public double getRemiseTotale(){
        return remise;
    };
    
    public double getPrixTotal(){
        double prix = 0;
        return prix;
    };
}
